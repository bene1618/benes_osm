Current state
=============

It is possible to import data into the postgis database with `docker-compose up import` currently; the default behavior is to
read data from `data.osm.pbf` and use `benes_osm2pgsql.style`. Both can be configured. If you do not want to install kosmtik
locally, you can use `docker-compose up kosmtik` instead.
