# Benes OSM style

A CartoCSS style for OSM data

## Getting started

* Download country shapefiles from Natural Earth
* Set up a PostgreSQL database with postgis and hstore extensions, e.g. using the docker-compose.yml file
* Run kosmtik either locally to serve tiles, or use the docker-compose.yml

## Producing bigger .osm.pbf files from smaller ones

Use osmconvert: e.g. `osmconvert64 schwaben-latest.osm.pbf --out-o5m | osmconvert64 - us-pacific-latest.osm.pbf
-o=joined.osm.pbf`
