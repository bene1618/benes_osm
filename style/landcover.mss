#land {
  ::outline {
    line-color: @water-boundaries;
    line-width: 2;
    line-join: round;
  }
  ::fill {
    polygon-fill: @land;
  }
}

#landcover-low-zoom [zoom >= 6], #landcover [zoom >= 11] {
  [landuse = 'commercial'] { 
    polygon-fill: @commercial; 
    [zoom >= 13] { polygon-fill: @commercial-highzoom; }
  }
  [landuse = 'industrial'] { 
    polygon-fill: @industrial; 
    [zoom >= 13] { polygon-fill: @industrial-highzoom; }
  }
  [landuse = 'residential'] {
    polygon-fill: @residential; 
    [zoom >= 13] { polygon-fill: @residential-highzoom; }
  }
  [landuse = 'retail'] { 
    polygon-fill: @retail; 
    [zoom >= 13] { polygon-fill: @retail-highzoom; }
  }

  [landuse = 'farmland'] { polygon-fill: @farmland; }
  [landuse = 'farmyard'] { polygon-fill: @farmyard; }

  [landuse = 'forest'],[natural = 'wood'] { polygon-fill: @forest; }
  [landuse = 'allotments'] { polygon-fill: @allotments; }
  [landuse = 'orchard'] { polygon-fill: @allotments; }
  [landuse = 'vineyard'] { polygon-fill: @allotments; }
  [landuse = 'grass'], [natural = 'grassland'] { polygon-fill: @grass; }
  [landuse = 'plant_nursery'] { polygon-fill: @plant_nursery; }
  [landuse = 'meadow'] { polygon-fill: @meadow; }
  [landuse = 'greenhouse_horticulture'] { polygon-fill: @plant_nursery; }

  [landuse = 'military'] { polygon-fill: @military; }

  [landuse = 'salt_pond'] { polygon-fill: @salt_pond; }

  [leisure = 'park'] { polygon-fill: @park; }
  [leisure = 'garden'] { polygon-fill: @garden; }

  [natural = 'beach'] { polygon-fill: @sand; }
  [natural = 'sand'] { polygon-fill: @sand; }
  [natural = 'heath'] { polygon-fill: @heath; }
  [natural = 'mud'] { polygon-fill: @mud; }
  [natural = 'wetland'] { polygon-fill: @water; }
  [natural = 'scree'] { polygon-fill: @scree; }
  [natural = 'shingle'] { polygon-fill: @shingle; }
  [natural = 'bare_rock'] { polygon-fill: @bare_rock; }
  [natural = 'scrub'] { polygon-fill: @scrub; }
}

#landcover [zoom >= 11] {
  [landuse = 'greenfield'] { polygon-fill: @greenfield; }
  [landuse = 'construction'] { polygon-fill: @construction; }
  [landuse = 'quarry'] { polygon-fill: @quarry; }
  [landuse = 'brownfield'] { polygon-fill: @brownfield; }
  [landuse = 'garages'] { polygon-fill: @garages; }
  [landuse = 'landfill'] { polygon-fill: @landfill; }

  [landuse = 'cemetery'] { polygon-fill: @grass; }
  [landuse = 'village_green'] { polygon-fill: @park; }
  [landuse = 'recreation_ground'] { polygon-fill: @park; }

  [landuse = 'religious'] { polygon-fill: @religious; }

  [leisure = 'dog_park'] { polygon-fill: @park; }
  [leisure = 'golf_course'] { polygon-fill: @grass; }
  [leisure = 'ice_rink'] { polygon-fill: @ice_rink; }
  [leisure = 'marina'] { 
    line-color: @marina-line; 
    line-width: 3
  }
  [leisure = 'miniature_golf'] { polygon-fill: @grass; }
  [leisure = 'pitch'] { polygon-fill: @grass; }
  [leisure = 'sports_centre'] { polygon-fill: @stadium; }
  [leisure = 'stadium'] { polygon-fill: @stadium; }
  [leisure = 'track'] { polygon-fill: @track; }

  [aeroway = 'apron'] { polygon-fill: @apron; }
/*  [aeroway = 'aerodrome'] { } */

  [man_made = 'works'] { polygon-fill: @works; }
  [man_made = 'wastewater_plant'] { polygon-fill: @works; }
  [man_made = 'water_works'] { polygon-fill: @works; }

  [power = 'plant'] { polygon-fill: @power; }
  [power = 'substation'] { polygon-fill: @power; }
  [power = 'generator'] { polygon-fill: @power; }

  [shop = 'mall'] { polygon-fill: @mall; }

  [tourism = 'camp_site'] { polygon-fill: @camp_site; }
  [tourism = 'caravan_site'] { polygon-fill: @camp_site; }
  [tourism = 'picnic_site'] { polygon-fill: @camp_site; }

  /*
  amenity: parking, bicycle_parking, motorcycle_parking, taxi, university, college, school, hospital, kindergarten, grave_yard,
           place_of_worship, prison, clinic, ferry_terminal, marketplace, community_centre, social_facility, arts_centre, 
           parking_space, bus_station, fire_station, police
  */
}
