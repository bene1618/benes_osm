@waterway-large-lowzoom-width: 0.7;
@waterway-large-z9-width: 1.2;
@waterway-large-z10-width: 1.6;
@waterway-large-z11-width: 2.4;
@waterway-large-z12-width: 3;
@waterway-large-z13-width: 3.5;
@waterway-large-z14-width: 5;
@waterway-large-z15-width: 8;
@waterway-large-z16-width: 10;

@waterway-small-z14-width: 1.8;
@waterway-small-z15-width: 2.3;
@waterway-small-z16-width: 3;

#water-areas {
  [waterway = 'dock'], [waterway = 'riverbank'],
  [landuse = 'basin'], [landuse = 'reservoir'],
  [natural = 'water'] {
    polygon-fill: @water;
  }
}

#water-lines {
  [waterway = 'river'][zoom >= 8], [waterway = 'canal'][zoom >= 10],
  [waterway = 'stream'][zoom >= 14], [waterway = 'drain'][zoom >= 14], [waterway = 'ditch'][zoom >= 14] {
    line-color: @water;
  
    [int_tunnel = 'no'] {
      line-join: round;
      line-cap: butt;
    }
    [int_tunnel = 'yes'] {
      line-dasharray: 4,2;
    }
  
    [waterway = 'river'], [waterway = 'canal'] {
      line-width: @waterway-large-lowzoom-width;
      [zoom >= 11] { line-width: @waterway-large-z11-width; }
      [zoom >= 12] { line-width: @waterway-large-z12-width; }
      [zoom >= 13] { line-width: @waterway-large-z13-width; }
      [zoom >= 14] { line-width: @waterway-large-z14-width; }
      [zoom >= 15] { line-width: @waterway-large-z15-width; }
      [zoom >= 16] { line-width: @waterway-large-z16-width; }
    }
  
    [waterway = 'stream'], [waterway = 'drain'], [waterway = 'ditch'] {
      line-width: @waterway-small-z14-width;
      [zoom >= 15] { line-width: @waterway-small-z15-width; }
      [zoom >= 16] { line-width: @waterway-small-z16-width; }
    }
  }
}

#water-area-names [zoom >= 7][way_pixels >= 200] {
  text-fill: @water-name;
  text-name: [name];
  text-face-name: 'Linux Biolinum O Regular';
  text-halo-fill: fadeout(white, 30%);
  text-halo-radius: 1;
  text-margin: 5;
  text-placement-type: simple;
  text-placements: "N,S,E,W,NW,SW,NE,SE";

  text-size: 9;
  [zoom >= 12] { text-size: 10; }
  [zoom >= 15] { text-size: 11; }
}

#water-line-names [waterway = 'river'][zoom >= 10] {
  text-fill: @water-name;
  text-placement: line;
  text-name: [name];
  text-face-name: 'Linux Biolinum O Regular';
  text-halo-fill: fadeout(white, 30%);
  text-halo-radius: 1;
  text-margin: 20;
  text-repeat-distance: 100;

  text-size: 9;
  [zoom >= 12] { text-size: 10; }
  [zoom >= 15] { text-size: 11; }
}

