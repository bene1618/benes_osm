#admin_boundaries {
  [admin_level = '1'], [admin_level = '2'] {
    [zoom <= 9] { line-width: 0.5; }
    [zoom >= 10] { line-width: 0.8; }
    line-color: @admin-boundaries;
  }
  [admin_level = '3'], [admin_level = '4'] {
    [zoom <= 9] { line-width:0.3; }
    [zoom >= 10] { line-width:0.7; }
    line-color: @admin-boundaries;
  }
  [zoom >= 10] {
    [admin_level = '5'],
    [admin_level = '6'] {
      line-width:0.3;
      line-color: @admin-boundaries;
    }
  }
}
