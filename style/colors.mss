/* Landcover */
@water: #b8dee6;
@water-boundaries: #85c5d3;
@land: #f7f2e8;
@admin-boundaries: #dbcba8;

@commercial: #eaa67f;
@commercial-highzoom: #eabba1;
@industrial: #d8a48f;
@industrial-highzoom: #d8b4a5;
@residential: #ecb4b9;
@residential-highzoom: #d3bfc2;
@retail: #f5a258;
@retail-highzoom: #f5bf90;

@greenfield: #e2e2cd;
@construction: #cbcbb8;
@quarry: #bec1bf;
@brownfield: #e2e2cd;
@garages: #b5b2b0;
@landfill: #a39c76;

@farmland: #def8c5;
@farmyard: #d1c87f;

@forest: #aac6a2;
@allotments: #b5be93;
@grass: #9ed48b;
@plant_nursery: #c6d6b8;
@meadow: #c5e6b9;

@military: #84a68d;
@religious: #868bba;
@salt_pond: #f0f0f0;

@park: #abf09f;
@garden: #76c583;

@ice_rink: #b6fefa;
@marina-line: #4e6fa3;
@stadium: #fafce0;
@track: #e77c61;

@sand: #f5f5a1;
@heath: #d0dfb3;
@mud: #dfdfbd;
@scree: #dfdfd4;
@shingle: #dfdfd4;
@bare_rock: #d0d4ce;
@scrub: #c9dfca;

@apron: #c5c5c5;
@works: #c5aa92;
@power: #c5b4c3;
@mall: #ac8565;
@camp_site: #aedd8e;

/* Roads */
@motorway-casing: #c7350c;
@motorway-lowzoom: #d79357;
@motorway: #ffe45d;

@trunk-casing: #f0c021;
@trunk-lowzoom: #c19a1a;
@trunk: #f1eb35;

@primary-casing: #f0c021;
@primary: #f1d498;

@secondary-casing: #b9aab6;
@secondary: #f1eee1;

@tertiary-casing: #88857e;
@tertiary: #f4f4ed;

@minor-road-casing: #e3e1de;
@minor-road: #fdfbf8;

@pedestrian: #c7c9de;
@pedestrian-casing: #8c8e9d;

@living-street: #fff7ed;

@raceway: #ec9d9f;

@footway: #8c8e9d;
@cycleway: #3eba29;
@bridleway: #afba30;
@track: #e3ded3;

@steps: #ff9e58;

/* Rail */

@rail-lowzoom: #575757;
@rail-dark: #7e7e7e;
@rail-light: #ffffff;

/* Buildings */
@building: #ad9b89;
@building-line: #958676;

/* Text */
@place-country: #5d5a53;
@place-medium: #2e2f38;
@place-small: #595b6c;
@place-halo: #fdfbf8;

@water-name: #1d8595;

@road-name: #353431;
@road-halo: #fffbec;
