#placenames-countries [zoom <= 7] {
  text-fill: @place-country;
  text-name: [NAME_DE];
  text-face-name: 'Linux Biolinum O Regular';
  text-halo-fill: fadeout(@place-halo, 30%);
  text-halo-radius: 2;
  text-margin: 30;
}

#placenames-medium [zoom <= 14] {
  [zoom >= 7][population >= 40000],
  [zoom >= 9][population >= 5000],
  [zoom >= 10] {
    text-fill: @place-medium;
    text-name: [name];
    text-face-name: 'Linux Biolinum O Regular';
    text-halo-fill: fadeout(@place-halo, 30%);
    text-halo-radius: 1;
    text-size: 9;

    text-margin: 30;
    [zoom >= 10] { text-margin: 60; }
    [zoom >= 11] { text-margin: 100; }
    [zoom >= 12] { text-margin: 150; }
    [population >= 1000] { text-size: 10; }
    [population >= 5000] { 
      text-size: 11; 
      text-halo-radius: 1.5;
    }
    [population >= 10000] { text-size: 12; }
    [population >= 40000] { 
      text-size: 14; 
      text-halo-radius: 2;
    }
    [population >= 100000] { text-size: 16; }
    [zoom >= 13] { 
      text-size: 14; 
      [population >= 10000] { text-size: 18; }
    }
  }
}

#placenames-small [zoom >= 13] [zoom <= 14] {
  text-fill: @place-small;
  text-name: [name];
  text-face-name: 'Linux Biolinum O Regular';
  text-halo-fill: fadeout(@place-halo, 30%);
  text-halo-radius: 2;
  text-margin: 50;
  text-size: 14;
}
