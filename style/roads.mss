/* Low zoom widths */
@motorway-width-small: 2;
@trunk-width-small: 2;
@primary-width-small: 1.7;
@secondary-width-small: 1;

/* Zoom level 10 */
@motorway-casing-z10-width: 3;
@trunk-casing-z10-width: 3;
@primary-casing-z10-width: 2;
@secondary-casing-z10-width: 1.5;
@tertiary-casing-z10-width: 1.2;

@motorway-z10-width: 1.5;
@trunk-z10-width: 1;
@primary-z10-width: 1.7;
@secondary-z10-width: 1;
@tertiary-z10-width: 1;

/* Zoom level 11 */

@motorway-casing-z11-width: 4;
@trunk-casing-z11-width: 4;
@primary-casing-z11-width: 2;
@secondary-casing-z11-width: 1.5;
@tertiary-casing-z11-width: 1.2;

@motorway-z11-width: 2;
@trunk-z11-width: 2;
@primary-z11-width: 1.7;
@secondary-z11-width: 1;
@tertiary-z11-width: 1;

/* Zoom level 12 */

@motorway-casing-z12-width: 5;
@trunk-casing-z12-width: 4;
@primary-casing-z12-width: 3;
@secondary-casing-z12-width: 2.5;
@tertiary-casing-z12-width: 1.2;
@residential-casing-z12-width: 0.8;

@motorway-z12-width: 2;
@trunk-z12-width: 2;
@primary-z12-width: 2.6;
@secondary-z12-width: 2;
@tertiary-z12-width: 1;

/* Zoom level 13 */

@motorway-casing-z13-width: 5.5;
@trunk-casing-z13-width: 5;
@primary-casing-z13-width: 4;
@secondary-casing-z13-width: 3;
@tertiary-casing-z13-width: 2.4;
@residential-casing-z13-width: 1.4;

@motorway-z13-width: 3;
@trunk-z13-width: 3;
@primary-z13-width: 3;
@secondary-z13-width: 2.5;
@tertiary-z13-width: 2;

/* Zoom level 14 */

@motorway-casing-z14-width: 6;
@trunk-casing-z14-width: 6;
@primary-casing-z14-width: 6;
@secondary-casing-z14-width: 5;
@tertiary-casing-z14-width: 3;
@residential-casing-z14-width: 2.5;
@pedestrian-casing-z14-width: 2;
@minor-road-casing-z14-width: 1;

@motorway-z14-width: 3;
@trunk-z14-width: 4;
@primary-z14-width: 5;
@secondary-z14-width: 4.5;
@tertiary-z14-width: 2;
@residential-z14-width: 1.5;
@pedestrian-z14-width: 1.6;
@minor-road-z14-width: 0.9;

/* Zoom level 15 */

@motorway-casing-z15-width: 15;
@trunk-casing-z15-width: 12;
@primary-casing-z15-width: 10;
@secondary-casing-z15-width: 8;
@tertiary-casing-z15-width: 8;
@residential-casing-z15-width: 5;
@pedestrian-casing-z15-width: 5;
@minor-road-casing-z15-width: 1.4;

@motorway-z15-width: 13;
@trunk-z15-width: 10;
@primary-z15-width: 8;
@secondary-z15-width: 6;
@tertiary-z15-width: 6;
@residential-z15-width: 4;
@pedestrian-z15-width: 4;
@minor-road-z15-width: 1.3;

/* Zoom level 16 */

@motorway-casing-z16-width: 18;
@trunk-casing-z16-width: 15;
@primary-casing-z16-width: 15;
@secondary-casing-z16-width: 12;
@tertiary-casing-z16-width: 10;
@residential-casing-z16-width: 8;
@pedestrian-casing-z16-width: 8;
@minor-road-casing-z16-width: 4;

@motorway-z16-width: 16;
@trunk-z16-width: 13;
@primary-z16-width: 13;
@secondary-z16-width: 10;
@tertiary-z16-width: 8;
@residential-z16-width: 7;
@pedestrian-z16-width: 7;
@minor-road-z16-width: 3;
@minor-service-z16-width: 2;

@shield-size: 10;
@shield-line-spacing: -1.50;
@shield-spacing: 760;
@shield-repeat-distance: 400;
@shield-margin: 20;
@shield-font: 'Linux Biolinum O Regular';

#roads, #bridges, #tunnels {
  ::casing {
    [zoom >= 10] {
      [highway = 'motorway'], [highway = 'motorway_link'] {
        line-width: @motorway-casing-z10-width;
        [zoom >= 11] { line-width: @motorway-casing-z11-width; }
        [zoom >= 12] { line-width: @motorway-casing-z12-width; }
        [zoom >= 13] { line-width: @motorway-casing-z13-width; }
        [zoom >= 14] { line-width: @motorway-casing-z14-width; }
        [zoom >= 15] { line-width: @motorway-casing-z15-width; }
        [zoom >= 16] { line-width: @motorway-casing-z16-width; }
        
        line-color: @motorway-casing;
        #roads {
          line-join: round;
          line-cap: round;
        }
        #bridges {
          line-join: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
      }

      [highway = 'trunk'], [highway = 'trunk_link'] {
        line-width: @trunk-casing-z10-width;
        [zoom >= 11] { line-width: @trunk-casing-z11-width; }
        [zoom >= 12] { line-width: @trunk-casing-z12-width; }
        [zoom >= 13] { line-width: @trunk-casing-z13-width; }
        [zoom >= 14] { line-width: @trunk-casing-z14-width; }
        [zoom >= 15] { line-width: @trunk-casing-z15-width; }
        [zoom >= 16] { line-width: @trunk-casing-z16-width; }
        
        line-color: @trunk-casing;
        #roads {
          line-join: round;
          line-cap: round;
        }
        #bridges {
          line-join: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
      }

      [highway = 'primary'], [highway = 'primary_link'] {
        line-width: @primary-casing-z10-width;
        [zoom >= 11] { line-width: @primary-casing-z11-width; }
        [zoom >= 12] { line-width: @primary-casing-z12-width; }
        [zoom >= 13] { line-width: @primary-casing-z13-width; }
        [zoom >= 14] { line-width: @primary-casing-z14-width; }
        [zoom >= 15] { line-width: @primary-casing-z15-width; }
        [zoom >= 16] { line-width: @primary-casing-z16-width; }
        
        line-color: @primary-casing;
        #roads {
          line-join: round;
          line-cap: round;
        }
        #bridges {
          line-join: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
      }

      [highway = 'secondary'], [highway = 'secondary_link'] {
        line-width: @secondary-casing-z10-width;
        [zoom >= 11] { line-width: @secondary-casing-z11-width; }
        [zoom >= 12] { line-width: @secondary-casing-z12-width; }
        [zoom >= 13] { line-width: @secondary-casing-z13-width; }
        [zoom >= 14] { line-width: @secondary-casing-z14-width; }
        [zoom >= 15] { line-width: @secondary-casing-z15-width; }
        [zoom >= 16] { line-width: @secondary-casing-z16-width; }
        
        line-color: @secondary-casing;
        #roads {
          line-join: round;
          line-cap: round;
        }
        #bridges {
          line-join: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
      }

      [highway = 'tertiary'], [highway = 'tertiary_link'][zoom >= 13] {
        line-width: @tertiary-casing-z10-width;
        [zoom >= 11] { line-width: @tertiary-casing-z11-width; }
        [zoom >= 12] { line-width: @tertiary-casing-z12-width; }
        [zoom >= 13] { line-width: @tertiary-casing-z13-width; }
        [zoom >= 14] { line-width: @tertiary-casing-z14-width; }
        [zoom >= 15] { line-width: @tertiary-casing-z15-width; }
        [zoom >= 16] { line-width: @tertiary-casing-z16-width; }
        
        line-color: @tertiary-casing;
        #roads {
          line-join: round;
          line-cap: round;
        }
        #bridges {
          line-join: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
      }

      [highway = 'residential'], [highway = 'unclassified'] {
        [zoom >= 12] {
          line-width: @residential-casing-z12-width;
          [zoom >= 13] { line-width: @residential-casing-z13-width; }
          [zoom >= 14] { line-width: @residential-casing-z14-width; }
          [zoom >= 15] { line-width: @residential-casing-z15-width; }
          [zoom >= 16] { line-width: @residential-casing-z16-width; }
          
          line-color: @minor-road-casing;
          #roads {
            line-join: round;
            line-cap: round;
          }
          #bridges {
            line-join: round;
          }
          #tunnels {
            line-dasharray: 4,2;
          }
        }
      }

      [highway = 'living_street'], [highway = 'road'], [highway = 'service'][int_service = 'normal'] {
        [zoom >= 14] {
          line-width: @minor-road-casing-z14-width;
          [zoom >= 15] { line-width: @minor-road-casing-z15-width; }
          [zoom >= 16] { line-width: @minor-road-casing-z16-width; }
          
          line-color: @minor-road-casing;
          #roads {
            line-join: round;
            line-cap: round;
          }
          #bridges {
            line-join: round;
          }
          #tunnels {
            line-dasharray: 4,2;
          }
        }
      }

      [highway = 'pedestrian'] {
        [zoom >= 14] {
          line-width: @pedestrian-casing-z14-width;
          [zoom >= 15] { line-width: @pedestrian-casing-z15-width; }
          [zoom >= 16] { line-width: @pedestrian-casing-z16-width; }
          
          line-color: @pedestrian-casing;
          #roads {
            line-join: round;
            line-cap: round;
          }
          #bridges {
            line-join: round;
          }
          #tunnels {
            line-dasharray: 4,2;
          }
        }
      }
    }
  }

  [int_railway = 'rail'][zoom >= 8] {
    [zoom <= 9] {
      line-color: @rail-lowzoom;
      line-width: 0.8;
      [zoom >= 8] { line-width: 1; }
      line-join: round;
    }
    [zoom >= 10] {
      #roads, #bridges {
        dark/line-join: round;
        light/line-color: @rail-light;
        light/line-join: round;
        [int_railway = 'rail'] {
          dark/line-color: @rail-dark;
          dark/line-width: 2;
          light/line-width: 0.75;
          light/line-dasharray: 8,8;
          [zoom >= 13] {
            dark/line-width: 3;
            light/line-width: 1;
          }
          [zoom >= 15] {
            light/line-dasharray: 0,8,8,1;
          }
          [zoom >= 18] {
            dark/line-width: 4;
            light/line-width: 2;
          }
        }
      }
    }
  }

  [highway = 'motorway'], [highway = 'motorway_link'] {
    line-width: @motorway-width-small;
    [zoom >= 10] { line-width: @motorway-z10-width; }
    [zoom >= 11] { line-width: @motorway-z11-width; }
    [zoom >= 12] { line-width: @motorway-z12-width; }
    [zoom >= 13] { line-width: @motorway-z13-width; }
    [zoom >= 14] { line-width: @motorway-z14-width; }
    [zoom >= 15] { line-width: @motorway-z15-width; }
    [zoom >= 16] { line-width: @motorway-z16-width; }
    
    line-color: @motorway;
    [zoom <= 9] { line-color: @motorway-lowzoom; }

    line-join: round;
    #roads { line-cap: round; }
    #tunnels { line-opacity: 0.6 }
  }

  [highway = 'trunk'], [highway = 'trunk_link'][zoom >= 7] {
    line-width: @trunk-width-small;
    [zoom >= 10] { line-width: @trunk-z10-width; }
    [zoom >= 11] { line-width: @trunk-z11-width; }
    [zoom >= 12] { line-width: @trunk-z12-width; }
    [zoom >= 13] { line-width: @trunk-z13-width; }
    [zoom >= 14] { line-width: @trunk-z14-width; }
    [zoom >= 15] { line-width: @trunk-z15-width; }
    [zoom >= 16] { line-width: @trunk-z16-width; }
    
    line-color: @trunk;
    [zoom <= 9] { line-color: @trunk-lowzoom; }

    line-join: round;
    #roads { line-cap: round; }
    #tunnels { line-opacity: 0.6 }
  }

  [highway = 'primary'][zoom >= 7], [highway = 'primary_link'][zoom >= 9] {
    line-width: @primary-width-small;
    [zoom >= 10] { line-width: @primary-z10-width; }
    [zoom >= 11] { line-width: @primary-z11-width; }
    [zoom >= 12] { line-width: @primary-z12-width; }
    [zoom >= 13] { line-width: @primary-z13-width; }
    [zoom >= 14] { line-width: @primary-z14-width; }
    [zoom >= 15] { line-width: @primary-z15-width; }
    [zoom >= 16] { line-width: @primary-z16-width; }
    
    line-color: @primary;
    [zoom <= 9] { line-color: @primary-casing; }


    line-join: round;
    #roads { line-cap: round; }
    #tunnels { line-opacity: 0.6 }
  }

  [highway = 'secondary'][zoom >= 9], [highway = 'secondary_link'][zoom >= 10] {
    line-width: @secondary-width-small;
    [zoom >= 10] { line-width: @secondary-z10-width; }
    [zoom >= 11] { line-width: @secondary-z11-width; }
    [zoom >= 12] { line-width: @secondary-z12-width; }
    [zoom >= 13] { line-width: @secondary-z13-width; }
    [zoom >= 14] { line-width: @secondary-z14-width; }
    [zoom >= 15] { line-width: @secondary-z15-width; }
    [zoom >= 16] { line-width: @secondary-z16-width; }
    
    line-color: @secondary;
    [zoom <= 9] { line-color: @secondary-casing; }

    line-join: round;
    #roads { line-cap: round; }
    #tunnels { line-opacity: 0.6 }
  }

  [highway = 'tertiary'][zoom >= 10], [highway = 'tertiary_link'][zoom >= 13] {
    line-width: @tertiary-z10-width;
    [zoom >= 11] { line-width: @tertiary-z11-width; }
    [zoom >= 12] { line-width: @tertiary-z12-width; }
    [zoom >= 13] { line-width: @tertiary-z13-width; }
    [zoom >= 14] { line-width: @tertiary-z14-width; }
    [zoom >= 15] { line-width: @tertiary-z15-width; }
    [zoom >= 16] { line-width: @tertiary-z16-width; }
    
    line-color: @tertiary;

    line-join: round;
    #roads { line-cap: round; }
    #tunnels { line-opacity: 0.6; }
  }

  [highway = 'residential'], [highway = 'unclassified'] {
    [zoom >= 14] {
      line-color: @minor-road;
      line-width: @residential-z14-width;
      [zoom >= 15] { line-width: @residential-z15-width; }
      [zoom >= 16] { line-width: @residential-z16-width; }

      line-join: round;
      #roads { line-cap: round; }
      #tunnels { line-opacity: 0.6; }
    }
  }

  [highway = 'living_street'] {
    [zoom >= 14] {
      line-color: @living-street;
      line-width: @minor-road-z14-width;
      [zoom >= 15] { line-width: @minor-road-z15-width; }
      [zoom >= 16] { line-width: @minor-road-z16-width; }

      line-join: round;
      #roads { line-cap: round; }
      #tunnels { line-opacity: 0.6; }
    }
  }

  [highway = 'road'], [highway = 'service'][int_service = 'normal'] {
    [zoom >= 14] {
      line-color: @minor-road;
      line-width: @minor-road-z14-width;
      [zoom >= 15] { line-width: @minor-road-z15-width; }
      [zoom >= 16] { line-width: @minor-road-z16-width; }

      line-join: round;
      #roads { line-cap: round; }
      #tunnels { line-opacity: 0.6; }
    }
  }

  [highway = 'pedestrian'] {
    [zoom >= 14] {
      line-color: @pedestrian;
      line-width: @pedestrian-z14-width;
      [zoom >= 15] { line-width: @pedestrian-z15-width; }
      [zoom >= 16] { line-width: @pedestrian-z16-width; }

      line-join: round;
      #roads { line-cap: round; }
      #tunnels { line-opacity: 0.6; }
    }
  }

  [highway = 'service'][int_service = 'minor'] {
    [zoom >= 16] {
      line-color: @minor-road;
      line-width: @minor-service-z16-width;

      line-join: round;
      #roads { line-cap: round; }
      #tunnels { line-opacity: 0.6; }
    }
  }

  [highway = 'raceway'] {
    [zoom >= 14] {
      line-color: @raceway;
      line-width: @minor-road-z14-width;
      [zoom >= 15] { line-width: @minor-road-z15-width; }
      [zoom >= 16] { line-width: @minor-road-z16-width; }

      line-join: round;
      #roads { line-cap: round; }
      #tunnels { line-opacity: 0.6; }
    }
  }

  [highway = 'steps'] {
    [zoom >= 14] {
      line-color: @steps;
      line-dasharray: 1,1;
      line-width: 1;
    }
  }

  [highway = 'footway'],
  [highway = 'path'][bicycle != 'designated'][horse != 'designated'] {
    [zoom >= 16] {
      line-color: @footway;
      line-width: 1;
      line-dasharray: 6,4;
    }
  }

  [highway = 'cycleway'],
  [highway = 'path'][bicycle = 'designated'] {
    [zoom >= 15] {
      line-color: @cycleway;
      line-width: 1;
      line-dasharray: 6,4;
    }
  }

  [highway = 'bridleway'],
  [highway = 'path'][horse = 'designated'] {
    [zoom >= 15] {
      line-color: @bridleway;
      line-width: 1;
      line-dasharray: 6,4;
    }
  }

  [highway = 'track'] {
    [zoom >= 14] {
      line-color: @track;
      line-join: round;
      line-width: 1;
    }
  }
}

#roads-area [zoom >= 14] {
  [highway = 'service'] { polygon-fill: @minor-road; }

  [highway = 'pedestrian'],[highway = 'footway'] { polygon-fill: @pedestrian; }
}

/*
#landcover [zoom >= 11] {
  [highway = 'services'] {}
  [highway = 'rest_area'] {}
  [railway = 'station'] {}
}
*/

#road-shields {
  [zoom >= 12][highway = 'primary'], [zoom >= 10][highway = 'trunk'], [zoom >= 9][highway = 'motorway'] {
    shield-file: url("svg/shields/motorway_[width]x[height].svg"); 
    [highway = 'trunk'] { shield-file: url("svg/shields/trunk_[width]x[height].svg"); }
    [highway = 'primary'] { shield-file: url("svg/shields/primary_[width]x[height].svg"); }

    shield-name: "[refs]";
    shield-size: @shield-size;
    shield-line-spacing: @shield-line-spacing;
    shield-placement: line;
    shield-spacing: @shield-spacing;
    shield-repeat-distance: @shield-repeat-distance;
    shield-margin: @shield-margin;
    shield-face-name: @shield-font;
    shield-clip: false;
  }
}

#road-area-names [zoom >= 16] {
  text-fill: @road-name;
  text-placement: point;
  text-name: [name];
  text-face-name: 'Linux Biolinum O Regular';
  text-margin: 30;
  text-clip: false;
  text-halo-fill: fadeout(@road-halo, 30%);
  text-halo-radius: 1;

  text-size: 10;
  [zoom >= 17] { text-size: 11; }
}

#road-names [zoom >= 13] {
  [highway = 'motorway'], [highway = 'trunk'], [highway = 'primary'], [highway = 'secondary'],
  [highway = 'tertiary'],
  [highway = 'unclassified'],
  [highway = 'residential'][zoom >= 14],
  [zoom >= 16] {
    text-fill: @road-name;
    text-placement: line;
    text-name: [name];
    text-face-name: 'Linux Biolinum O Regular';
    text-margin: 5;
    text-clip: false;
    text-halo-fill: fadeout(@road-halo, 30%);
    text-halo-radius: 1;

    text-size: 9;
    [highway = 'secondary'], [highway = 'tertiary'] { text-size: 8; }
    [highway = 'unclassified'], [highway = 'residential'] { text-size: 7; }
    [zoom >= 16] { text-size: 10; }
    [zoom >= 17] { text-size: 11; }

    text-spacing: 70;
    text-repeat-distance: 30;

    [zoom >= 17] {
      text-spacing: 100;
      text-repeat-distance: 60;
    }

    [zoom >= 18] {
      text-spacing: 200;
      text-repeat-distance: 140;
    }
  }
}
