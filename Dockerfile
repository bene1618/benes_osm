FROM ubuntu:bionic

RUN apt-get update && apt-get install --no-install-recommends -y \
    ca-certificates curl gnupg postgresql-client python3 python3-distutils \
    fonts-hanazono fonts-noto-cjk fonts-noto-hinted fonts-noto-unhinted \
    mapnik-utils nodejs npm ttf-unifont unzip fonts-linuxlibertine && \
    rm -rf /var/lib/apt/lists/*

RUN npm set prefix /usr &&  npm -g install kosmtik

CMD ["kosmtik", "serve",  "--host", "0.0.0.0", "/osm-style/project.mml"]

